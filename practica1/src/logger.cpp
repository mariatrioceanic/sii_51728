#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>

using namespace std;

#define TAM 61

int main()
{
   int estado_crear, estado_lectura, estado_cerrar, estado_unlink, estado_leer;
   char buff[TAM];
  
  //crear tuberia
  estado_crear=mkfifo("/tmp/fifo",0777);
  if(estado_crear==-1){
      perror("error mkfifo");
      exit(1);
  }

  //Apertura de la tuberia en modo lectura
  estado_lectura=open("/tmp/fifo",O_RDONLY); //si lo abre bien devuelve el nuevo descriptor de fichero
   if(estado_lectura==-1){
      perror("error de apertura");
      exit(1);
   } 

  while(1)
{
  estado_leer=read(estado_lectura,buff,TAM*sizeof(char));
  if (estado_leer<=0){
     perror("error al leer del buffer");
     exit(2);
  }
  write(1,buff,TAM*sizeof(char)); //1-- salida estandar
}
    
  estado_unlink=unlink("/tmp/fifo");
  estado_cerrar=close(estado_lectura);
  if (estado_unlink==-1){
     perror("error al eliminar");
     exit(1);
  }
}
